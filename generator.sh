#!/bin/bash
#set -x
# check for dir given as cmd arg.
# otherwise exit with usage message

echo "
###############################################
# config generator for 0xff router
# for debian based systems
#
# $(basename ${0}) ${*}
#
# by Markus Gschwendt
# for Funkfeuer Vienna Wireless Community, 0xFF
# License: GPL
###############################################"

if [[ -z "${_DIR}" ]]; then
  _DIR="$(pwd)"
fi

if [[ -d "${_DIR}/${1}" ]] ; then
  CFG_DIR=${_DIR}/${1%/}
else
  echo "directory '${_DIR}/${1}' does not exist"
  echo -e "\nUsage: basename(${0}) <work directory>\n\n"
  exit 1
fi

. lib/common ${CFG_DIR}
if [[ -n "${ERR_CFG}" ]]; then
  echo -e "\n\nERROR: ${ERR_CFG}"
  exit 1
fi

. lib/hostname ${CFG_DIR}
. lib/if0 ${CFG_DIR}
. lib/radvd ${CFG_DIR}
. lib/nft ${CFG_DIR}
. lib/dnsmasq ${CFG_DIR}
. lib/olsr ${CFG_DIR}
. lib/olsr2 ${CFG_DIR}
. lib/vlan_watch ${CFG_DIR}
