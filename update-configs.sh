#!/bin/bash

DEFAULT_DIR="example"

DIRS="$(find . -name "config_*" -type d)"
for DIR in ${DIRS}; do
  cp ${DEFAULT_DIR}/deploy.sh ${DIR}
  cp ${DEFAULT_DIR}/update_kernel.sh ${DIR}
  cp ${DEFAULT_DIR}/resolv.conf ${DIR}
  cp ${DEFAULT_DIR}/defines.nft ${DIR}
  cp ${DEFAULT_DIR}/filter-local4.nft ${DIR}
  cp ${DEFAULT_DIR}/filter-local6.nft ${DIR}
  cp ${DEFAULT_DIR}/nftables.conf ${DIR}
done
