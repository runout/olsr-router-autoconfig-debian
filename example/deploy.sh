#!/bin/bash

if [[ ! -x "/usr/sbin/olsrd2_static" ]]; then
  echo "FATAL: missing olsrd2"
fi

if [[ -z "$(apt list nftables | grep installed)" ]]; then
  apt-get -y install nftables
fi

if [[ -n "$(apt list iptables | grep installed)" ]]; then
  apt-get -y purge iptables*
fi

if [[ -n "$(apt list resolvconf | grep installed)" ]]; then
  apt-get -y purge resolvconf
fi

if [[ -z "$(apt list olsrd | grep installed)" ]]; then
  apt-get -y install nftables olsrd
fi

if [[ -z "$(apt list dnsmasq | grep installed)" ]]; then
  apt-get -y install dnsmasq
  service dnsmasq stop
fi

if [[ -f "radvd.conf" ]]; then
  if [[ -z "$(apt list radvd | grep installed)" ]]; then
    apt-get -y install radvd
  fi
else
  apt-get -y purge radvd
fi

##########
# change prompt to include node name
##########
sed -i "s/\s\sPS1.*/  PS1='$\{debian_chroot:+\(\$debian_chroot\)\}\\\u@$\(hostname -f\)\\\n:\\\w\\\\$ '/g" /etc/bash.bashrc

##########
# copy hosts, hostname, dnsmasq, resolv config files
##########
cp hosts /etc/hosts
cp hostname /etc/hostname
cp dnsmasq.conf /etc/dnsmasq.d/dnsmasq.conf
cp resolv.conf /etc/resolv.conf

##########
# copy network config files
##########
mv /etc/network/interfaces /etc/network/interfaces.$(date +%Y%m%d-%H%M)
cp interfaces /etc/network/

##########
# copy radvd config files
##########
mv /etc/radvd.conf /etc/radvd.conf.$(date +%Y%m%d-%H%M)
if [[ -f "radvd.conf" ]]; then
  cp radvd.conf /etc/
fi

##########
# copy nft config files
##########
if [[ -d "/etc/nftables" ]]; then
  mkdir -p /var/backups/nftables/$(date +%Y%m%d-%H%M)
  mv /etc/nftables* /var/backups/nftables/$(date +%Y%m%d-%H%M)/
fi

mkdir -p /etc/nftables/{conf.d,rules4,rules6}
cp nftables.conf /etc/nftables.conf
cp defines.nft /etc/nftables/defines.nft
cp filter-local4.nft /etc/nftables/rules4/filter-local.nft
cp filter-nat.nft /etc/nftables/rules4/filter-nat.nft
cp filter-local6.nft /etc/nftables/rules6/filter-local.nft
update-rc.d nft remove
sed -i "s/^#\sProvides:.*/# Provides:          nft/g" /etc/init.d/nft
update-rc.d nft defaults enable

if [[ -z "$(grep 'nft \-f ' /etc/rc.local)" ]]; then
  sed -E -i 's/^[[:blank:]]*exit\s0[[:blank:]]*$/nft \-f \/etc\/nftables.conf\n\nexit 0/' /etc/rc.local
fi

##########
# copy olsr config files
##########
mkdir -p /var/www/olsrd2
echo "0xFF router" > /var/www/olsrd2/index.html
cp olsrd.conf /etc/olsrd/
cp olsrd2.conf /etc/olsrd2/

# patch olsr startscript. olsrd must start after olsrd2
update-rc.d olsrd remove
sed -i "s/^#\sRequired-Start.*$/# Required-Start:    \$network \$remote_fs \$syslog olsrd2/; s/^#\sRequired-Stop.*$/# Required-Stop:     \$network \$remote_fs \$syslog olsrd2/" /etc/init.d/olsrd
update-rc.d olsrd defaults enable

echo "You should reboot now!"
