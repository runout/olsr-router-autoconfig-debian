#!/bin/bash

if [[ ! -f "${1}" ]]; then
  echo "usage: $0 Armbian_XX.XX.X_Bananapir2_buster_legacy_X.X.X_minimal.img

    get and unpack the image like that:
        wget https://redirect.armbian.com/bananapir2/Buster_legacy_minimal --content-disposition
        xz -T 0 -d Armbian*.xz
"
  exit 1
fi

echo "mount image 'mount -o loop,offset=$((512 * 8192)) ${1} /mnt_img'"
mkdir -p /mnt_img
mount -o loop,offset=$((512 * 8192)) ${1} /mnt_img 

echo "backup boot to /boot-$(uname -r) and copy boot"
mv /boot /boot-$(uname -r)
cp -a /mnt_img/boot /boot
chown root:root -R /boot

echo "copy modules"
cp -a /mnt_img/lib/modules/* /lib/modules/
chown root:root -R /lib/modules/*

sync
umount /mnt_img
rm -r /mnt_img

echo "fix UUID in /boot/armbianEnv.txt"
cd /boot
sed -i "s/^rootdev=UUID=.*/rootdev=UUID=$(lsblk --output "UUID,MOUNTPOINT" | grep '\s\/$' | cut -d ' ' -f1)/g" armbianEnv.txt

sync
