# Autoconfig for OLSRv2 routers running Debian

This is mainly developed for the BananaPI-r2 running Debian Stable. But it should work on other Hardware too.

Make sure you are using _sysvinit_ instead of _systemd_!

Be aware that funkfeuer DNS zones are updated only at: 06:00 12:00 18:00.
This is important when you configure a 4in6 IP on portal.funkfeuer.at and use the autoconfig feature from this script which queries the DNS record for the 4in6-IPv4.

## work directory

The name of the _work directory_ has to start with `config_` followed by the node or device name. This is important, as the latter will be used as node name.

Examples: `config_nodename_devicenumber`, `config_node0001_01`, `config_node0001_02`

## Usage generator.sh

`bash generator.sh <work directory>`

The _work directory_  must already exist and contain a file called _config_.

Example _config_ file is _example/config_.

## Usage update-configs.sh

`bash update-configs.sh`

This script copies some static files from the example directory to _all_ existing config directories.

## Usage deploy.sh

`bash deploy.sh`

This script resides in the config directory of the node and copies the various files to the expected locations on a debian system. Additionally backups of the original `/etc/network/interfaces` will be created in `/etc/network/` and of `/etc/nftables*` to `/var/backups/nftables`

If necessary this script will install `dnsmasq` and `radvd`. It will remove `resolvconf`.

The start script of OLSRdv1 will be patched to start _after_ OLSRDv2. This avoids a problem with the default route to 4in6 interfaces when OLSRDv1 will be stoped.

## Usage update_kernel.sh

`bash update_kernel.sh <Armbian_Image.img>

This script resides in the config directory of the node and updates the kernel from a given image wich has to be downloaded and unpacked bevorehand.

* Backup the old `/boot` directory to `/boot-$(uname -r)`
* Copy the new `/boot` directory from the image
* Copy the new kernel modules from the image to `/lib/modules`
* Replace the UUID of the boot device in `/boot/armbianEnv.txt`

After running the script you must reboot the machine.

## HOWTO

* For OLSRv2 with 4in6 support: Configure a 4in6 IP on portal.funkfeuer.at (several hours before using this script). The name must be in the format _<DEVICEID>-4in6_.
* Create a new directory for each router/device. Important: See the `work directory` section above for correct naming.
* Copy the example config file to this new `work directory`
* Edit the `config` file. The documentaton is inside this file.
* Run the script with the work directory name as argument. `bash generator.sh <work directory>`
* The output/config files will be written to the work directory
* For the OLSRv2 http plugin the directory _/var/www/olsrd2/_ is expected to exist on the target router

Default values are configured in _default_ in the _example_ directory. This will be sourced before the config file. It accepts the same Variables as the _config_ file. Variables in the _default_ file will be overwritten by the variables in the _config_ file from your `work directory`.

## TODO

* Sanity check for values from the config file. There are absolutely no checks right now.
* Improof the output for OLSRD and OLSRD2 config.
* In the example config are multiple IF variables. At the moment only _IF00_ is used.
* Improof firewall rules.
* Allow multiple IPv6-ra on several interfaces.
